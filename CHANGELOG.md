# Changelog

## [Unreleased]

### Added

- Fichier `CHANGELOG.md`

### Changed

- Compatible SPIP 4.2.0-dev
